package com.example.demo;

import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@ToString
public class User {
    private String name;
    private String email;
    private String password;
    private String gender;
    private String note;
    private boolean married;
    @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
    private LocalDate birthday;
    private String profession;
}
