package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Slf4j
public class UserControllere {
    @PostMapping("/register")
    @ResponseBody // trả response trong body JSON
    // th:object="${user}" ràng buộc với @ModelAttribute("user") User user
    public ResponseEntity<User> submitForm(@ModelAttribute("user") User user) {
        log.info("{}", user);

        // trả đata kiểu REST API - JSON, dùng ResponseEntity
        return ResponseEntity.ok(user);
    }

    // điều hướng về form đăng kí, để rằng buộc vởi th:object="${user} : new_user.html
    @GetMapping("/registerForm")
    public String showForm(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "new_user";
    }
}
