package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {

    @GetMapping("/")
    public String hello(){
        return "hello";
    }

    @GetMapping("/hello")
    @ResponseBody
    public String hello2(){
        return "hello helo";
    }

    @GetMapping("/thymeleaf")
    public String thymeleaf(){
        return "hello";
    }

    @GetMapping("/thymeleaf-2")
    public String thymeleaf2(){
        return "hello2";
    }
}
