package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.List;

@Controller
@Slf4j
public class CustomerController {

    List<Customer> customers = List.of(
            Customer.builder().id(1).name("Jon").birthDay(LocalDate.now()).build(),
            Customer.builder().id(2).name("Dozs").birthDay(LocalDate.now()).email("he@gmail.com").build(),
            Customer.builder().id(3).name("Myli").birthDay(LocalDate.now()).build()
    );

    @GetMapping("customers")
    public ModelAndView customers() {
        ModelAndView customersViewModel = new ModelAndView("customers");
        customersViewModel.addObject("customers", customers);

        return customersViewModel;
    }

    @PostMapping(value = "/customer/add", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public void addCustomer(@ModelAttribute Customer customer) {
        log.info("{}", customer);
    }

    @GetMapping("customer/view")
    public ModelAndView customerView() {
        ModelAndView customersViewModel = new ModelAndView("new_customer");

        return customersViewModel;
    }

    @GetMapping("hello2.html")
    public String hello2() {
        return "hello2";
    }
}
