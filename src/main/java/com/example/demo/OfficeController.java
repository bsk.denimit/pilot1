package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Optional;

@Controller
public class OfficeController {
    @Autowired
    private OfficeRepository officeRepository;

    @GetMapping("/offices")
    @ResponseBody
    public ResponseEntity<List<Offices>> list() {
        return ResponseEntity.ok(officeRepository.findAll());
    }

    @GetMapping("/office/{id}")
    @ResponseBody
    public ResponseEntity<?> getOfficeById(@PathVariable("id") String id) {
        Optional<Offices> officeOp = officeRepository.findById(id);

        return officeOp.isPresent()
                ? ResponseEntity.ok(officeOp.get())
                : new ResponseEntity<>("Not foud office id: " + id, HttpStatus.NOT_FOUND);
    }

}
