package com.example.demo;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDate;

@Data
@Builder
@ToString
public class Customer {
    private int id;
    private String name;
    private LocalDate birthDay;
    private String email;
}
